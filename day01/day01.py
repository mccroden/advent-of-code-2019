#!/usr/bin/env python

from typing import List

modules = './input.txt'

# Part 1

def get_modules(filename: str) -> List[int]:
    result = []
    with open(modules, 'r') as f:
        result = [int(row.rstrip()) for row in f.readlines()]
    return result

def fuel_req(mass: int) -> int:
    req = mass // 3
    req -= 2
    return req

def total_req(modules: List[int]) -> int:
    return sum([fuel_req(module) for module in modules])

def part1():
    assert fuel_req(12) == 2
    assert fuel_req(14) == 2
    assert fuel_req(1969) == 654
    assert fuel_req(100756) == 33583
    print(f"Part 1: {total_req(get_modules(modules))}")

# Part 2

def fuel_req2(mass: int) -> int:
    total_fuel = 0
    while True:
        req = fuel_req(mass)
        if req <= 0:
            break
        else:
            total_fuel += req
            mass = req
    return total_fuel

def total_req2(modules: List[int]) -> int:
    return sum([fuel_req2(module) for module in modules])

def part2():
    assert fuel_req2(14) == 2
    assert fuel_req2(1969) == 966
    assert fuel_req2(100756) == 50346
    print(f"Part 2: {total_req2(get_modules(modules))}")

if __name__ == "__main__":
    part1()
    part2()
