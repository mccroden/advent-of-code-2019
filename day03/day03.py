#!/usr/bin/env python

from typing import List, Tuple, Set, Union
from collections import namedtuple

Point = namedtuple('Point', 'x y')

def wire(path_instructions: str) -> List[Point]:
    path = []
    instructions = path_instructions.split(',')
    coord = Point(0, 0)
    for instruction in instructions:
        direction, movement = instruction[0], int(instruction[1:])
        for i in range(movement):
            if direction == 'U':
                coord = Point(coord.x, coord.y + 1)
            elif direction == 'D':
                coord = Point(coord.x, coord.y - 1)
            elif direction == 'R':
                coord = Point(coord.x + 1, coord.y)
            elif direction == 'L':
                coord = Point(coord.x - 1, coord.y)
            else:
                raise ValueError(f"Invalid direction: {direction}")
            path.append(coord)
    return path

def find_crossed_wires(wire_1: List[Point], wire_2: List[Point]) -> Set[Point]:
    s1 = set(wire_1)
    s2 = set(wire_2)
    return s1.intersection(s2)

def manhattan(point1: Point, point2: Point) -> int:
    return abs(point1.x - point2.x) + abs(point1.y - point2.y)

def closest_to_point(origin_point: Point, points: Union[List[Point], Set[Point]]) -> int:
    distances = []
    for point in points:
        distances.append(manhattan(origin_point, point))
    return min(distances)

def read_input(filepath: str) -> str:
    with open(filepath, 'r') as f:
        inp = f.read()
    return inp

def part1():
    a1a = wire('R75,D30,R83,U83,L12,D49,R71,U7,L72')
    a1b = wire('U62,R66,U55,R34,D71,R55,D58,R83')
    crossed = find_crossed_wires(a1a, a1b)
    assert closest_to_point(Point(0,0), crossed) == 159
    a2a = wire('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51')
    a2b = wire('U98,R91,D20,R16,D67,R40,U7,R15,U6,R7')
    crossed = find_crossed_wires(a2a, a2b)
    assert closest_to_point(Point(0,0), crossed) == 135
    # challenge
    w1_filepath = 'wire1.txt'
    w2_filepath = 'wire2.txt'
    w1_inp = read_input(w1_filepath)
    w2_inp = read_input(w2_filepath)
    w1 = wire(w1_inp)
    w2 = wire(w2_inp)
    crossed = find_crossed_wires(w1, w2)
    print(f"Part 1: distance = {closest_to_point(Point(0,0), crossed)}")


# Part 2

def steps_to_point(to_point: Point, wire: List[Point]) -> int:
    steps = 0
    for p in wire:
        steps += 1
        if p == to_point:
            break
    return steps

def closest_signal_distance(wire1: List[Point], wire2: List[Point]) -> int:
    crossed = find_crossed_wires(wire1, wire2)
    distances = []
    for cross in crossed:
        s1 = steps_to_point(cross, wire1)
        s2 = steps_to_point(cross, wire2)
        distances.append(s1 + s2)
    return min(distances)



def part2():
    a = wire('R75,D30,R83,U83,L12,D49,R71,U7,L72')
    b = wire('U62,R66,U55,R34,D71,R55,D58,R83')
    assert closest_signal_distance(a, b) == 610

    a = wire('R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51')
    b = wire('U98,R91,D20,R16,D67,R40,U7,R15,U6,R7')
    assert closest_signal_distance(a, b) == 410

    # challenge
    w1_filepath = 'wire1.txt'
    w2_filepath = 'wire2.txt'
    w1_inp = read_input(w1_filepath)
    w2_inp = read_input(w2_filepath)
    w1 = wire(w1_inp)
    w2 = wire(w2_inp)
    closest = closest_signal_distance(w1, w2)
    print(f"Part 2: closest = {closest}")

    return


if __name__ == '__main__':
    part1()
    part2()
