# Advent of Code 2019

```
    _       _                 _            __    ____          _
   / \   __| |_   _____ _ __ | |_    ___  / _|  / ___|___   __| | ___
  / _ \ / _` \ \ / / _ \ '_ \| __|  / _ \| |_  | |   / _ \ / _` |/ _ \
 / ___ \ (_| |\ V /  __/ | | | |_  | (_) |  _| | |__| (_) | (_| |  __/
/_/   \_\__,_| \_/ \___|_| |_|\__|  \___/|_|    \____\___/ \__,_|\___|
```

| Description | Code |
|-------------|------|
| [The Tyranny of the Rocket Equation](https://adventofcode.com/2019/day/1) | [day01](/day01) |
| [1202 Program Alarm](https://adventofcode.com/2019/day/2) | [day02](/day02) |
| [Crossed Wires](https://adventofcode.com/2019/day/3) | [day03](/day03) |
| [day 04](https://adventofcode.com/2019/day/4) | [day04](/day04) |
| [day 05](https://adventofcode.com/2019/day/5) | [day05](/day05) |
| [day 06](https://adventofcode.com/2019/day/6) | [day06](/day06) |
| [day 07](https://adventofcode.com/2019/day/7) | [day07](/day07) |
| [day 08](https://adventofcode.com/2019/day/8) | [day08](/day08) |
| [day 09](https://adventofcode.com/2019/day/9) | [day09](/day09) |
| [day 10](https://adventofcode.com/2019/day/10) | [day10](/day10) |
| [day 11](https://adventofcode.com/2019/day/11) | [day11](/day11) |
| [day 12](https://adventofcode.com/2019/day/12) | [day12](/day12) |
| [day 13](https://adventofcode.com/2019/day/13) | [day13](/day13) |
| [day 14](https://adventofcode.com/2019/day/14) | [day14](/day14) |
| [day 15](https://adventofcode.com/2019/day/15) | [day15](/day15) |
| [day 16](https://adventofcode.com/2019/day/16) | [day16](/day16) |
| [day 17](https://adventofcode.com/2019/day/17) | [day17](/day17) |
| [day 18](https://adventofcode.com/2019/day/18) | [day18](/day18) |
| [day 19](https://adventofcode.com/2019/day/19) | [day19](/day19) |
| [day 20](https://adventofcode.com/2019/day/20) | [day20](/day20) |
| [day 21](https://adventofcode.com/2019/day/21) | [day21](/day21) |
| [day 22](https://adventofcode.com/2019/day/22) | [day22](/day22) |
| [day 22](https://adventofcode.com/2019/day/23) | [day23](/day23) |
| [day 24](https://adventofcode.com/2019/day/24) | [day24](/day24) |
| [day 25](https://adventofcode.com/2019/day/25) | [day25](/day25) |
| [day 26](https://adventofcode.com/2019/day/26) | [day26](/day26) |
| [day 27](https://adventofcode.com/2019/day/27) | [day27](/day27) |
| [day 28](https://adventofcode.com/2019/day/28) | [day28](/day28) |
| [day 29](https://adventofcode.com/2019/day/29) | [day29](/day29) |
| [day 30](https://adventofcode.com/2019/day/30) | [day30](/day30) |
| [day 31](https://adventofcode.com/2019/day/31) | [day31](/day31) |
