#!/usr/bin/env pythonLTi

from typing import Dict, List, Optional, Tuple
from enum import Enum, IntEnum

class Param(Enum):
    POSITION = 0
    IMMEDIATE = 1

class Opcode(IntEnum):
    ADD = 1
    MULTIPLY = 2
    STDIN = 3
    STDOUT = 4
    JUMPIFTRUE = 5
    JUMPIFFALSE = 6
    LESSTHAN = 7
    EQUALS = 8
    HALT = 99

class IntcodeComputer:

    def __init__(self, program: List[int], stdinput: Optional[int]):
        self.program = program
        self.stdinput = stdinput

    def _num_of_params_needed(self, opcode: Opcode) -> int:
        if opcode in [Opcode.ADD, Opcode.MULTIPLY, Opcode.LESSTHAN, Opcode.EQUALS]:
            return 3
        elif opcode in [Opcode.JUMPIFTRUE, Opcode.JUMPIFFALSE]:
            return 2
        elif opcode in [Opcode.STDIN, Opcode.STDOUT]:
            return 1
        elif opcode == Opcode.HALT:
            return 0
        else:
            return 0

    def _parse_opcode(self, opcode: int) -> Tuple[Opcode, List[int]]:
        sopcode = str(opcode)
        opcode = int(sopcode[-2:])
        op = Opcode(opcode)
        params_needed = self._num_of_params_needed(op)
        reverse_opcode = list(sopcode[:-2])
        reverse_opcode.reverse()
        if len(reverse_opcode) > 0:
            params = [int(x) for x in reverse_opcode]
        else:
            params = []
        params.extend([0]*4)
        params = params[:params_needed]
        return op, params

    def _get_value(self, idx: int, param_mode: Param) -> int:
        """Used to translate idx into a value or reference and return value."""
        if param_mode == Param.POSITION:
            return self.program[idx]
        elif param_mode == Param.IMMEDIATE:
            return idx
        else:
            raise ValueError(f"Invalid param_mode given: {param_mode}")
            return

    def _get_values(self, indexes: List[int], param_modes: List[int]) -> List[int]:
        result = []
        for idx, param_mode in zip(indexes, param_modes):
            result.append(self._get_value(idx, Param(param_mode)))
        return result

    def run(self):
        i = 0
        std_io = self.stdinput
        while True:
            opcode, param_modes = self._parse_opcode(self.program[i])
            print(f"{i=}: {opcode=} {param_modes=}")
            if opcode == Opcode.ADD:
                values = self._get_values(self.program[i+1:i+4], param_modes)
                value = values[0] + values[1]
                if param_modes[-1] == 0:
                    self.program[self.program[i+3]] = value
                else:
                    self.program[i+3] = value
                i += len(param_modes) + 1

            elif opcode == Opcode.MULTIPLY:
                values = self._get_values(self.program[i+1:i+4], param_modes)
                value = values[0] * values[1]
                #self.program[i+3] = value
                if param_modes[-1] == 0:
                    self.program[self.program[i+3]] = value
                else:
                    self.program[i+3] = value
                i += len(param_modes) + 1

            elif opcode == Opcode.STDIN:
                value = std_io
                print(f"{value=}")
                if param_modes[-1] == 0:
                    self.program[self.program[i+1]] = value
                else:
                    self.program[i+1] = value
                i += len(param_modes) + 1

            elif opcode == Opcode.STDOUT:
                value = self._get_values([self.program[i+1]], param_modes)
                std_io = value
                print(f"Output: std_io {std_io}")
                i += len(param_modes) + 1

            elif opcode == Opcode.JUMPIFTRUE:
                # jump to 2nd param value
                values = self._get_values(self.program[i+1:i+3], param_modes)
                if values[0] != 0:
                    i = values[1]
                else:
                    i += len(param_modes) + 1

            elif opcode == Opcode.JUMPIFFALSE:
                # jump to 2nd param value
                values = self._get_values(self.program[i+1:i+3], param_modes)
                if values[0] == 0:
                    i = values[1]
                else:
                    i += len(param_modes) + 1

            elif opcode == Opcode.LESSTHAN:
                values = self._get_values(self.program[i+1:i+4], param_modes)
                if param_modes[-1] == 0:
                    self.program[self.program[i+3]] = 1 if values[0] < values[1] else 0
                else:
                    self.program[i+1] = 1 if values[0] == values[1] else 0
                i += len(param_modes) + 1

            elif opcode == Opcode.EQUALS:
                values = self._get_values(self.program[i+1:i+4], param_modes)
                if param_modes[-1] == 0:
                    self.program[self.program[i+3]] = 1 if values[0] == values[1] else 0
                else:
                    self.program[i+1] = 1 if values[0] == values[1] else 0
                print(f"{values=}")
                i += len(param_modes) + 1

            elif opcode == Opcode.HALT:
                break

            else:
                raise ValueError(f"Invalid Opcode given {opcode}")

        return

    def should_halt(self, opcode: Opcode) -> bool:
        return opcode == Opcode.HALT


def get_intcode(filepath: str) -> List[int]:
    sintcode: List[str] = []
    with open(filepath, 'r') as f:
        sintcode = f.read().rstrip().split(',')
    intcode = [int(x) for x in sintcode]
    return intcode

def part1():
    intcode = get_intcode('input.txt')
    program = IntcodeComputer(intcode, 1)
    program.run()
    return

def part2():

    #intcode = [3,9,8,9,10,9,4,9,99,-1,8]
    #intcode = [3,3,1108,-1,8,3,4,3,99]
    #intcode = [4,3,1108,-1,8,3,4,3,99]
    #program = IntcodeComputer(intcode, 8)
    #program.run()
    #intcode = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
    #    1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
    #    999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]
    #program = IntcodeComputer(intcode, 5)
    #program.run()

    intcode = get_intcode('input.txt')
    program = IntcodeComputer(intcode, 5)
    program.run()


if __name__ == '__main__':
    part2()
