#!/usr/bin/env python

from typing import Dict, Tuple, List

def parse_file(orbit_map_path: str) -> List[List[str]]:
    with open(orbit_map_path, 'r') as f:
        return parse(f.read().strip())


def parse(orbit_map: str) -> List[List[str]]:
    o_map = orbit_map.split('\n')
    return [orbit.strip().split(')') for orbit in o_map]

def create_planets(orbit_map: List[List[str]]) -> Dict[str, Dict[str, List[str]]]:
    orbits: Dict[str, Dict[str, List[str]]] = {}
    for orbiter, orbitee in orbit_map:
        if orbiter not in orbits.keys():
            orbits[orbiter] = {'orbits': [], 'orbited_by': []}
        orbits[orbiter]["orbited_by"].append(orbitee)
        if orbitee not in orbits.keys():
            orbits[orbitee] = {'orbits': [], 'orbited_by': []}
        orbits[orbitee]["orbits"].append(orbiter)
    return orbits

def count_direct_orbit(orbit_map: Dict[str, Dict[str, List[str]]], planet: str) -> int:
    return len(orbit_map[planet]['orbits']) if orbit_map[planet] else 0

def count_all_orbits(orbit_map: Dict[str, Dict[str, List[str]]], planet: str) -> int:
    return len(get_all_orbits(orbit_map, planet))

def count_indirect_orbits(orbit_map: Dict[str, Dict[str, List[str]]], planet: str) -> int:
    direct_count = count_direct_orbit(orbit_map, planet)
    all_count = count_all_orbits(orbit_map, planet)
    return all_count - direct_count

def get_all_orbits(orbit_map: Dict[str, Dict[str, List[str]]], planet: str) -> List[str]:
    all_orbits: List[str] = []
    while (more_orbits := orbit_map[planet]['orbits']):
        all_orbits.extend(more_orbits)
        planet = more_orbits[0]
    return all_orbits

def count_orbits(orbit_map: Dict[str, Dict[str, List[str]]]) -> dict:
    return {}

def orbit_count_for_all_planets(orbit_map: Dict[str, Dict[str, List[str]]]) -> int:
    total: int = 0
    for planet in orbit_map.keys():
        total += count_all_orbits(orbit_map, planet)
    return total

def part1():
    in_1 = ("""COM)B
           B)C
           C)D
           D)E
           E)F
           B)G
           G)H
           D)I
           E)J
           J)K
           K)L""")
    omap = parse(in_1)
    planets = create_planets(omap)
    total = 0
    for k in planets.keys():
        total += count_all_orbits(planets, k)
    assert total == 42

    orbit_map = create_planets(parse_file('./input.txt'))
    print(f"Part 1: Count = {orbit_count_for_all_planets(orbit_map)}")

    return orbit_map

def find_first_occurrence_from_lists(list1: List[str], list2: List[str]) -> str:
    #list_a, list_b = list1, list2 if len(list1) <= len(list2) else list2, list1
    lista = list1[:]
    listb = list2[:]
    lista.reverse()
    listb.reverse()
    for items in zip(lista, listb):
        if items[0] != items[1]:
            break
        planet = items[0]
    return planet

def part2(orbit_map: Dict[str, Dict[str, List[str]]]):
    # get orbit back to COM for YOU and SAN, remove commonalities except for first one
    san_orbits = get_all_orbits(orbit_map, 'SAN')
    you_orbits = get_all_orbits(orbit_map, 'YOU')
    common_orbits: set = set(you_orbits).intersection(set(you_orbits))
    first_common_planet = find_first_occurrence_from_lists(san_orbits, you_orbits)
    san_orbits_for_transfer = san_orbits[:san_orbits.index(first_common_planet)]
    you_orbits_for_transfer = you_orbits[:you_orbits.index(first_common_planet)]
    print(f"Part 2: Orbital Transfers = {len(san_orbits_for_transfer + you_orbits_for_transfer)}")
    return


if __name__ == '__main__':
    orbit_map = part1()
    part2(orbit_map)
