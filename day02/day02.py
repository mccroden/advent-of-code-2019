#!/usr/bin/env python

from typing import List

intcode_path = './input.txt'

def get_intcode(filepath: str) -> List[int]:
    intcode = []
    with open(filepath, 'r') as f:
        intcode = f.read().rstrip().split(',')
    intcode = [int(x) for x in intcode]
    return intcode

def calculate(opcode: int, values: list) -> int:
    if opcode == 1:
        result = sum(values)
    elif opcode == 2:
        result = values[0] * values[1]
    else:
        raise ValueError
    return result

def should_halt(intcode_slice: List[int]) -> bool:
    if intcode_slice[0] == 99 or len(intcode_slice) < 4:
        return True
    else:
        return False

def program(code: List[int]) -> List[int]:
    intcode = code.copy()
    for i in range(0, len(intcode), 4):
        temp_intcode = intcode[i:i+4]
        if should_halt(temp_intcode):
            break
        else:
            values = [intcode[temp_intcode[1]], intcode[temp_intcode[2]]]
            calc_value = calculate(temp_intcode[0], values)
            intcode[temp_intcode[3]] = calc_value

    return intcode

def set_prior_state(intcode: List[int], noun: int =12, verb: int = 2) -> List[int]:
    prior_state = intcode.copy()
    prior_state[1] = noun
    prior_state[2] = verb
    return prior_state

def part1():
    assert program([1,9,10,3,2,3,11,0,99,30,40,50]) == [3500,9,10,70,2,3,11,0,99,30,40,50]
    assert program([1,0,0,0,99]) == [2, 0, 0, 0, 99]
    assert program([2,3,0,3,99]) == [2,3,0,6,99]
    assert program([2,4,4,5,99,0]) == [2,4,4,5,99,9801]
    assert program([1,1,1,4,99,5,6,0,99]) == [30,1,1,4,2,5,6,0,99]
    intcode = get_intcode(intcode_path)
    prior_intcode = set_prior_state(intcode)
    result = program(prior_intcode)
    print(f"Part 1: {result[0]}")

# Part 2

def part2(goal_seek: int = 19690720):
    intcode = get_intcode(intcode_path)
    for n in range(0,100):
        for v in range(0, 100):
            if program(set_prior_state(intcode, n, v))[0] == goal_seek:
                print(f"Part 2: noun = {n} verb = {v}")
                print(f"100 * noun + verb = {100*n + v}")
                return

if __name__ == '__main__':
    part1()
    part2()
