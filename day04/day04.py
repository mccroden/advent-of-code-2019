#!/usr/bin/env python

from typing import Tuple

def is_not_a_decrease(num1: str, num2: str) -> bool:
    return int(num1) <= int(num2)

def has_repeat_digits(password: int) -> bool:
    pswd = str(password)
    return any([pswd[i] == pswd[i+1] for i in range(len(pswd) - 1)])

def has_no_decrease(password: int) -> bool:
    pswd = str(password)
    return all(is_not_a_decrease(pswd[i], pswd[i+1]) for i in range(len(pswd) - 1))


def generate_passwords(bounds: Tuple = (125730, 579381)) -> list:
    # Constraints
    # It is a six-digit number.
    # The value is within the range given in your puzzle input.
    # Two adjacent digits are the same (like 22 in 122345).
    # Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
    passwords = []
    for idx in range(bounds[0], bounds[1]):
        if has_repeat_digits(idx) and has_no_decrease(idx):
            passwords.append(idx)
    return passwords

def part1():
    passwords = generate_passwords()
    print(f"Part 1: # of passwords = {len(passwords)}")

# Part 2

def has_only_2_repeat(password: int) -> bool:
    # one set of repeat numbers can only be length 2.
    if has_repeat_digits(password):
        pswd = str(password)
        counts = []
        for num in list(set(pswd)):
            cnt = sum([num == p for p in pswd])
            counts.append(cnt)
        return 2 in counts

def generate_passwords2(bounds: Tuple = (125730, 579381)) -> list:
    # Constraints
    # It is a six-digit number.
    # The value is within the range given in your puzzle input.
    # Two adjacent digits are the same (like 22 in 122345).
    # Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).
    passwords = []
    for idx in range(bounds[0], bounds[1]):
        if has_only_2_repeat(idx) and has_no_decrease(idx):
            passwords.append(idx)
    return passwords

def part2():
    passwords = generate_passwords2()
    print(f"Part 2: # of passwords = {len(passwords)}")

if __name__ == '__main__':
    part1()
    part2()
